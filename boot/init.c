#include "boot/boot.h"
#include "boot/uart.h"
#include "kern/defs.h"
#include "kern/image.h"

#define INIT_STACK_SIZE 0x1000
char boot_cpu_stack[PLAT_CPU_NUMBER][INIT_STACK_SIZE] ALIGN(16);

#define NOT_BSS (0xBEEFUL)
uint64_t spin_table[PLAT_CPU_NUMBER] = {NOT_BSS};

volatile uint64_t clear_bss_flag = NOT_BSS;

static void clear_bss(void) {
  uint64_t bss_start_addr;
  uint64_t bss_end_addr;
  uint64_t i;

  bss_start_addr = (uint64_t)&_bss_start;
  bss_end_addr = (uint64_t)&_bss_end;

  for (i = bss_start_addr; i < bss_end_addr; ++i) *(char *)i = 0;

  clear_bss_flag = 0;
}

void early_init(void) {
  /* Clear the bss area for the kernel image */
  clear_bss();

  /* Initialize UART before enabling MMU. */
  early_uart_init();
  early_uart_send_string("  _    _                         ____   _____ " "\r\n");
  early_uart_send_string(" | |  | |                       / __ \\ / ____|" "\r\n");
  early_uart_send_string(" | |__| | ___  _   _  __ _  ___| |  | | (___  " "\r\n");
  early_uart_send_string(" |  __  |/ _ \\| | | |/ _` |/ _ \\ |  | |\\___ \\ " "\r\n");
  early_uart_send_string(" | |  | | (_) | |_| | (_| |  __/ |__| |____) |" "\r\n");
  early_uart_send_string(" |_|  |_|\\___/ \\__,_|\\__, |\\___|\\____/|_____/ " "\r\n");
  early_uart_send_string("                      __/ |                   " "\r\n");
  early_uart_send_string("                     |___/                    " "\r\n");
  early_uart_send_string("HougeOS is booting...\r\n");

  /* Initialize Boot Page Table. */
  early_uart_send_string("[BOOT] Install boot page table\r\n");
  init_boot_pt();

  early_uart_send_string("[BOOT] Before enable el1 MMU\r\n");

  /* Enable MMU. */
  el1_mmu_activate();
  early_uart_send_string("[BOOT] Enable el1 MMU\r\n");

  /* Call Kernel Main. */
  early_uart_send_string("[BOOT] Jump to kernel main\r\n");

  start_kernel(spin_table);
  while (1) {;}

  /* Never reach here */
}

void secondary_early_init(uint64_t cpuid) {
  el1_mmu_activate();
  start_kernel_secondary(cpuid);
}
