#include "boot/uart.h"
#include "boot/boot.h"

#if defined(SOC_BCM283X)

#define PHYSADDR_OFFSET     0x3F000000UL

#define GPFSEL1             (PHYSADDR_OFFSET + 0x00200004)
#define GPSET0              (PHYSADDR_OFFSET + 0x0020001C)
#define GPCLR0              (PHYSADDR_OFFSET + 0x00200028)
#define GPPUD               (PHYSADDR_OFFSET + 0x00200094)
#define GPPUDCLK0           (PHYSADDR_OFFSET + 0x00200098)

#define AUX_ENABLES         (PHYSADDR_OFFSET + 0x00215004)
#define AUX_MU_IO_REG       (PHYSADDR_OFFSET + 0x00215040)
#define AUX_MU_IER_REG      (PHYSADDR_OFFSET + 0x00215044)
#define AUX_MU_IIR_REG      (PHYSADDR_OFFSET + 0x00215048)
#define AUX_MU_LCR_REG      (PHYSADDR_OFFSET + 0x0021504C)
#define AUX_MU_MCR_REG      (PHYSADDR_OFFSET + 0x00215050)
#define AUX_MU_LSR_REG      (PHYSADDR_OFFSET + 0x00215054)
#define AUX_MU_MSR_REG      (PHYSADDR_OFFSET + 0x00215058)
#define AUX_MU_SCRATCH      (PHYSADDR_OFFSET + 0x0021505C)
#define AUX_MU_CNTL_REG     (PHYSADDR_OFFSET + 0x00215060)
#define AUX_MU_STAT_REG     (PHYSADDR_OFFSET + 0x00215064)
#define AUX_MU_BAUD_REG     (PHYSADDR_OFFSET + 0x00215068)

void early_uart_init(void) {
  unsigned int ra;

  ra = early_get32(GPFSEL1);
  ra &= ~(7 << 12);
  ra |= 2 << 12;
  ra &= ~(7 << 15);
  ra |= 2 << 15;
  early_put32(GPFSEL1, ra);

  early_put32(GPPUD, 0);
  delay(150);
  early_put32(GPPUDCLK0, (1 << 14) | (1 << 15));
  delay(150);
  early_put32(GPPUDCLK0, 0);

  early_put32(AUX_ENABLES, 1);
  early_put32(AUX_MU_IER_REG, 0);
  early_put32(AUX_MU_CNTL_REG, 0);
  early_put32(AUX_MU_IER_REG, 0);
  early_put32(AUX_MU_LCR_REG, 3);
  early_put32(AUX_MU_MCR_REG, 0);
  early_put32(AUX_MU_BAUD_REG, 270);

  early_put32(AUX_MU_CNTL_REG, 3);
}

unsigned int early_uart_lsr(void) { return early_get32(AUX_MU_LSR_REG); }

static void early_uart_send(unsigned int c) {
  while (1) {
    if (early_uart_lsr() & 0x20) break;
  }
  early_put32(AUX_MU_IO_REG, c);
}

void early_uart_send_string(char *str) {
  for (int i = 0; str[i] != '\0'; i++) early_uart_send((char)str[i]);
}

#elif defined(SOC_BCM2711)

#define PHYSADDR_OFFSET     0x0FE000000UL

#define GPFSEL1             (PHYSADDR_OFFSET + 0x00200004)
#define GPSET0              (PHYSADDR_OFFSET + 0x0020001C)
#define GPCLR0              (PHYSADDR_OFFSET + 0x00200028)
#define GPPUD               (PHYSADDR_OFFSET + 0x002000e4)

#define AUX_ENABLES         (PHYSADDR_OFFSET + 0x00215004)
#define AUX_MU_IO_REG       (PHYSADDR_OFFSET + 0x00215040)
#define AUX_MU_IER_REG      (PHYSADDR_OFFSET + 0x00215044)
#define AUX_MU_IIR_REG      (PHYSADDR_OFFSET + 0x00215048)
#define AUX_MU_LCR_REG      (PHYSADDR_OFFSET + 0x0021504C)
#define AUX_MU_MCR_REG      (PHYSADDR_OFFSET + 0x00215050)
#define AUX_MU_LSR_REG      (PHYSADDR_OFFSET + 0x00215054)
#define AUX_MU_MSR_REG      (PHYSADDR_OFFSET + 0x00215058)
#define AUX_MU_SCRATCH      (PHYSADDR_OFFSET + 0x0021505C)
#define AUX_MU_CNTL_REG     (PHYSADDR_OFFSET + 0x00215060)
#define AUX_MU_STAT_REG     (PHYSADDR_OFFSET + 0x00215064)
#define AUX_MU_BAUD_REG     (PHYSADDR_OFFSET + 0x00215068)

#define AUX_MU_BAUD(baud)   ((SOC_CLOCK/(baud*8))-1)

void early_uart_init(void) {
  unsigned int ra;

  ra = early_get32(GPFSEL1);
  ra &= ~(7 << 12);
  ra |= 2 << 12;
  ra &= ~(7 << 15);
  ra |= 2 << 15;
  early_put32(GPFSEL1, ra);

  early_put32(GPPUD, 0);

  early_put32(AUX_ENABLES, 1);
  early_put32(AUX_MU_IER_REG, 0);
  early_put32(AUX_MU_CNTL_REG, 0);
  early_put32(AUX_MU_IER_REG, 0);
  early_put32(AUX_MU_LCR_REG, 3);
  early_put32(AUX_MU_MCR_REG, 0);
  early_put32(AUX_MU_BAUD_REG, AUX_MU_BAUD(115200));

  early_put32(AUX_MU_CNTL_REG, 3);
}

unsigned int early_uart_lsr(void) { return early_get32(AUX_MU_LSR_REG); }

static void early_uart_send(unsigned int c) {
  while (1) {
    if (early_uart_lsr() & 0x20) break;
  }
  early_put32(AUX_MU_IO_REG, c);
}

void early_uart_send_string(char *str) {
  for (int i = 0; str[i] != '\0'; i++) early_uart_send((char)str[i]);
}

#else

#error "No compatible SoC macro defined for boot/uart.c"

#endif
