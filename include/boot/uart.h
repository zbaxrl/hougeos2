#pragma once

void early_put32(unsigned long int addr, unsigned int ch);
unsigned int early_get32(unsigned long int addr);
void delay(unsigned long time);

void early_uart_init(void);
void early_uart_send_string(char *s);
