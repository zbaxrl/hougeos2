#pragma once
#include <kern/defs.h>

void uart_init(void);

uint32_t uart_rx(void);
uint32_t uart_rx_available(void);

void uart_tx(uint32_t);
uint32_t uart_tx_available(void);
