#pragma once

// Assuming LP64 Model
typedef unsigned long     uint64_t;
typedef unsigned int      uint32_t;
typedef unsigned short    uint16_t;
typedef unsigned char     uint8_t;

typedef long              int64_t;
typedef int               int32_t;
typedef short             int16_t;
typedef char              int8_t;

typedef uint64_t          paddr_t;
typedef uint64_t          vaddr_t;
