#pragma once
#include <kern/defs.h>

// --------------------------
// Special instructions
// --------------------------

// Get current PE id
static inline uint64_t hal_cpuid(void) {
  uint64_t x;
  asm volatile("mrs %0, mpidr_el1" : "=r" (x) );
  x &= 0xff;
  return x;
}

// wait for event
static inline void hal_wfe(void) {
  asm volatile("wfe");
}

// send event
static inline void hal_sev(void) {
  asm volatile("sev");
}

// --------------------------
// MMIO
// --------------------------

static inline void mmio_write32(uint64_t reg, uint32_t val) { *(volatile uint32_t *) reg = val; }
static inline uint32_t mmio_read32(uint64_t reg) { return *(volatile uint32_t *) reg; }
static inline void mmio_write64(uint64_t reg, uint64_t val) { *(volatile uint64_t *) reg = val; }
static inline uint64_t mmio_read64(uint64_t reg) { return *(volatile uint64_t *) reg; }
static inline void mmio_fence(void) { __sync_synchronize(); }

// --------------------------
// Atomic
// --------------------------

void hal_lock32(volatile uint32_t *);
void hal_unlock32(volatile uint32_t *);

void hal_atomic_inc32(volatile uint32_t *ptr);
