#pragma once
#include <kern/defs.h>

typedef struct {
  uint32_t locked;
  char *name;
} spinlock_t;

void spinlock_init    (spinlock_t *lock, char *name);
void spinlock_acquire (spinlock_t *lock);
void spinlock_release (spinlock_t *lock);
