#pragma once

#define SZ_16K          0x4000
#define SZ_64K          0x10000

#define KERNEL_VADDR    0xffff000000000000
#define TEXT_OFFSET     0x200000
