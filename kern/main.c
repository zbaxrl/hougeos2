#include <kern/arm.h>
#include <kern/image.h>
#include <kern/macro.h>
#include <kern/uart.h>
#include <kern/printf.h>
#include <kern/hal.h>
#include <kern/spinlock.h>

ALIGN(KERNEL_STACK_SIZE)
char kernel_stack[PLAT_CPU_NUM][KERNEL_STACK_SIZE];

volatile uint32_t lock_disable;
static spinlock_t lock;

void kmain_secondary(void) {
  uint64_t cpuid = hal_cpuid();
  printf("Hello from CPU %d\r\n", (uint32_t) cpuid);

  while (1);
}

void kmain(paddr_t spin_table) {
  lock_disable = 1;
  uart_init();
  printf_init();
  spinlock_init(&lock, "main lock");

  printf("  _    _                         ____   _____ " "\r\n");
  printf(" | |  | |                       / __ \\ / ____|" "\r\n");
  printf(" | |__| | ___  _   _  __ _  ___| |  | | (___  " "\r\n");
  printf(" |  __  |/ _ \\| | | |/ _` |/ _ \\ |  | |\\___ \\ " "\r\n");
  printf(" | |  | | (_) | |_| | (_| |  __/ |__| |____) |" "\r\n");
  printf(" |_|  |_|\\___/ \\__,_|\\__, |\\___|\\____/|_____/ " "\r\n");
  printf("                      __/ |                   " "\r\n");
  printf("                     |___/                    " "\r\n");
  printf("Core 0 has booted.\r\n");

  printf("Spin table located at PA %p\r\n", spin_table);

  printf("Wakeup other cores...\r\n");

  lock_disable = 0;

  for (uint32_t i = 0; i < PLAT_CPU_NUM; i++) {
    vaddr_t st_addr = spin_table + i * 8;
    mmio_write64(st_addr, 1);
    hal_sev();
  }

  while (1);
}
