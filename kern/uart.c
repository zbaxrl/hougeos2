#include <kern/hal.h>
#include <kern/image.h>
#include <kern/uart.h>
#include <kern/spinlock.h>
#include <kern/arm.h>

static spinlock_t uart_lock;

#if defined(SOC_BCM283X)

#define PHYSADDR_OFFSET   (KERNEL_VADDR + 0x3F000000UL)

#define GPFSEL1           (PHYSADDR_OFFSET + 0x00200004)
#define GPSET0            (PHYSADDR_OFFSET + 0x0020001C)
#define GPCLR0            (PHYSADDR_OFFSET + 0x00200028)
#define GPPUD             (PHYSADDR_OFFSET + 0x00200094)
#define GPPUDCLK0         (PHYSADDR_OFFSET + 0x00200098)

#define AUX_ENABLES       (PHYSADDR_OFFSET + 0x00215004)
#define AUX_MU_IO_REG     (PHYSADDR_OFFSET + 0x00215040)
#define AUX_MU_IER_REG    (PHYSADDR_OFFSET + 0x00215044)
#define AUX_MU_IIR_REG    (PHYSADDR_OFFSET + 0x00215048)
#define AUX_MU_LCR_REG    (PHYSADDR_OFFSET + 0x0021504C)
#define AUX_MU_MCR_REG    (PHYSADDR_OFFSET + 0x00215050)
#define AUX_MU_LSR_REG    (PHYSADDR_OFFSET + 0x00215054)
#define AUX_MU_MSR_REG    (PHYSADDR_OFFSET + 0x00215058)
#define AUX_MU_SCRATCH    (PHYSADDR_OFFSET + 0x0021505C)
#define AUX_MU_CNTL_REG   (PHYSADDR_OFFSET + 0x00215060)
#define AUX_MU_STAT_REG   (PHYSADDR_OFFSET + 0x00215064)
#define AUX_MU_BAUD_REG   (PHYSADDR_OFFSET + 0x00215068)

void uart_init(void) {
  spinlock_init(&uart_lock, "uart_lock");

  unsigned int ra;

  ra = mmio_read32(GPFSEL1);
  ra &= ~(7 << 12);
  ra |= 2 << 12;
  ra &= ~(7 << 15);
  ra |= 2 << 15;
  mmio_write32(GPFSEL1, ra);

  mmio_write32(GPPUD, 0);
  // delay(150);
  mmio_write32(GPPUDCLK0, (1 << 14) | (1 << 15));
  // delay(150);
  mmio_write32(GPPUDCLK0, 0);

  mmio_write32(AUX_ENABLES, 1);
  mmio_write32(AUX_MU_IER_REG, 0);
  mmio_write32(AUX_MU_CNTL_REG, 0);
  mmio_write32(AUX_MU_IER_REG, 0);
  mmio_write32(AUX_MU_LCR_REG, 3);
  mmio_write32(AUX_MU_MCR_REG, 0);
  mmio_write32(AUX_MU_BAUD_REG, 270);

  mmio_write32(AUX_MU_CNTL_REG, 3);

  /* Clear the screen */
  uart_tx(12);
  uart_tx(27);
  uart_tx('[');
  uart_tx('2');
  uart_tx('J');
}

#elif defined(SOC_BCM2711)

#define PHYSADDR_OFFSET     (KERNEL_VADDR + 0x0FE000000UL)

#define GPFSEL1             (PHYSADDR_OFFSET + 0x00200004)
#define GPSET0              (PHYSADDR_OFFSET + 0x0020001C)
#define GPCLR0              (PHYSADDR_OFFSET + 0x00200028)
#define GPPUD               (PHYSADDR_OFFSET + 0x002000e4)

#define AUX_ENABLES         (PHYSADDR_OFFSET + 0x00215004)
#define AUX_MU_IO_REG       (PHYSADDR_OFFSET + 0x00215040)
#define AUX_MU_IER_REG      (PHYSADDR_OFFSET + 0x00215044)
#define AUX_MU_IIR_REG      (PHYSADDR_OFFSET + 0x00215048)
#define AUX_MU_LCR_REG      (PHYSADDR_OFFSET + 0x0021504C)
#define AUX_MU_MCR_REG      (PHYSADDR_OFFSET + 0x00215050)
#define AUX_MU_LSR_REG      (PHYSADDR_OFFSET + 0x00215054)
#define AUX_MU_MSR_REG      (PHYSADDR_OFFSET + 0x00215058)
#define AUX_MU_SCRATCH      (PHYSADDR_OFFSET + 0x0021505C)
#define AUX_MU_CNTL_REG     (PHYSADDR_OFFSET + 0x00215060)
#define AUX_MU_STAT_REG     (PHYSADDR_OFFSET + 0x00215064)
#define AUX_MU_BAUD_REG     (PHYSADDR_OFFSET + 0x00215068)

#define AUX_MU_BAUD(baud)   ((SOC_CLOCK/(baud*8))-1)

void uart_init(void) {
  spinlock_init(&uart_lock, "uart_lock");

  unsigned int ra;

  ra = mmio_read32(GPFSEL1);
  ra &= ~(7 << 12);
  ra |= 2 << 12;
  ra &= ~(7 << 15);
  ra |= 2 << 15;
  mmio_write32(GPFSEL1, ra);

  mmio_write32(GPPUD, 0);

  mmio_write32(AUX_ENABLES, 1);
  mmio_write32(AUX_MU_IER_REG, 0);
  mmio_write32(AUX_MU_CNTL_REG, 0);
  mmio_write32(AUX_MU_IER_REG, 0);
  mmio_write32(AUX_MU_LCR_REG, 3);
  mmio_write32(AUX_MU_MCR_REG, 0);
  mmio_write32(AUX_MU_BAUD_REG, AUX_MU_BAUD(115200));

  mmio_write32(AUX_MU_CNTL_REG, 3);

  uart_tx(12);
  uart_tx(27);
  uart_tx('[');
  uart_tx('2');
  uart_tx('J');
}

#else

#error "No compatible SoC macro defined for kern/uart.c"

#endif

uint32_t uart_lsr(void) { return mmio_read32(AUX_MU_LSR_REG); }

uint32_t uart_rx(void) {
  spinlock_acquire(&uart_lock);
  while (1) {
    if (uart_lsr() & 0x01) break;
  }

  uint32_t ret = mmio_read32(AUX_MU_IO_REG) & 0xFF;
  spinlock_release(&uart_lock);
  return ret;
}

uint32_t uart_rx_available(void) {
  return !!(uart_lsr() & 0x01);
}

void uart_tx(uint32_t c) {
  spinlock_acquire(&uart_lock);
  while (1) {
    if (uart_lsr() & 0x20) break;
  }
  mmio_write32(AUX_MU_IO_REG, c);
  spinlock_release(&uart_lock);
}

uint32_t uart_tx_available(void) {
  return !!(uart_lsr() & 0x20);
}
