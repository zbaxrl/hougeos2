#include <kern/hal.h>
#include <kern/spinlock.h>

// lock_disable is provided by main.c
// spinlock won't be available before we setup kvm.
// So we need a bypass.
extern volatile uint32_t lock_disable;

void spinlock_init(spinlock_t *lock, char *name) {
  lock->locked = 0;
  lock->name = name;
}

void spinlock_acquire(spinlock_t *lock) {
  if (lock_disable) return;
  hal_lock32(&lock->locked);
}

void spinlock_release(spinlock_t *lock) {
  if (lock_disable) return;
  hal_unlock32(&lock->locked);
}
