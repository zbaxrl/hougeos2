#include <kern/defs.h>
#include <kern/uart.h>
#include <kern/printf.h>
#include <kern/spinlock.h>
#include <stdarg.h>

static char digits[] = "0123456789abcdef";

static spinlock_t printf_lock;

static void printint(int32_t xx, int32_t base, int32_t sign) {
  char buf[16];
  int32_t i;
  uint32_t x;

  if(sign && (sign = xx < 0))
    x = -xx;
  else
    x = xx;

  i = 0;
  do {
    buf[i++] = digits[x % base];
  } while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
    uart_tx(buf[i]);
}

static void printptr(uint64_t x) {
  int32_t i;
  uart_tx('0');
  uart_tx('x');
  for (i = 0; i < (sizeof(uint64_t) * 2); i++, x <<= 4)
    uart_tx(digits[x >> (sizeof(uint64_t) * 8 - 4)]);
}

// Print to the console. only understands %d, %x, %p, %s.
void printf(char *fmt, ...) {
  spinlock_acquire(&printf_lock);

  va_list ap;
  int32_t i, c;
  char *s;

  if (fmt == 0)
    panic("null fmt");

  va_start(ap, fmt);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
    if(c != '%'){
      uart_tx(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
    switch(c){
    case 'd':
      printint(va_arg(ap, int32_t), 10, 1);
      break;
    case 'x':
      printint(va_arg(ap, int32_t), 16, 1);
      break;
    case 'p':
      printptr(va_arg(ap, uint64_t));
      break;
    case 's':
      if((s = va_arg(ap, char*)) == 0)
        s = "(null)";
      for(; *s; s++)
        uart_tx(*s);
      break;
    case '%':
      uart_tx('%');
      break;
    default:
      // Print unknown % sequence to draw attention.
      uart_tx('%');
      uart_tx(c);
      break;
    }
  }

  spinlock_release(&printf_lock);
}

void panic(char *s) {
  printf("panic: ");
  printf(s);
  printf("\n");
  while(1);
}

void printf_init(void) {
  spinlock_init(&printf_lock, "printf_lock");
}
