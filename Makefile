CROSS = aarch64-linux-gnu-
# for RPi 3 and QEMU
# TARGET = BCM283X
# for RPi 4
TARGET = BCM2711
QEMU_NCPU = 4
INCLUDE_FLAGS = -Iinclude
CFLAGS = -Wall -mabi=lp64 -march=armv8-a -mcpu=cortex-a72 -O2 -ffreestanding -nostdlib -nostartfiles -mno-outline-atomics $(INCLUDE_FLAGS)
CFLAGS += -DSOC_$(TARGET)
CFLAGS += -DRPI_FIRMWARE_SPINTABLE

BOOT_CFILES = $(wildcard boot/*.c)
BOOT_SFILES = $(wildcard boot/*.S)
BOOT_OFILES = $(BOOT_SFILES:.S=.o) $(BOOT_CFILES:.c=.o)

KERN_CFILES = $(wildcard kern/*.c)
KERN_SFILES = $(wildcard kern/*.S)
KERN_OFILES = $(KERN_SFILES:.S=.o) $(KERN_CFILES:.c=.o)

GDB_PORT = 1234

all: clean hougeos2.img

boot/%.o: boot/%.c
	$(CROSS)gcc $(CFLAGS) -fPIC -c $< -o $@

boot/%.o: boot/%.S
	$(CROSS)gcc $(CFLAGS) -fPIC -c $< -o $@

kern/%.o: kern/%.c
	$(CROSS)gcc $(CFLAGS) -c $< -o $@

kern/%.o: kern/%.S
	$(CROSS)gcc $(CFLAGS) -c $< -o $@

kern/kern.lds: kern/kern.lds.in
	$(CROSS)gcc -E -x c $(INCLUDE_FLAGS) -DBOOT_OFILES="${BOOT_OFILES}" kern/kern.lds.in | grep -v "^#" > kern/kern.lds

hougeos2.img: $(BOOT_OFILES) $(KERN_OFILES) kern/kern.lds
	$(CROSS)ld $(BOOT_OFILES) $(KERN_OFILES) -T kern/kern.lds -e _start -o hougeos2.elf
	$(CROSS)objcopy -O binary hougeos2.elf hougeos2.img

dist: hougeos2.img
	mkimage -A arm64 -T kernel -C none -a 0x200000 -e 0x200000 -n "hougeos2" -d hougeos2.img hougeos2.uimg
	sudo cp hougeos2.img /srv/tftp/hougeos2.img
	sudo cp hougeos2.uimg /srv/tftp/hougeos2.uimg

disa: hougeos2.elf
	$(CROSS)objdump -d hougeos2.elf > hougeos2.S

qemu: hougeos2.elf
	qemu-system-aarch64 -machine raspi3 -nographic -serial null -serial mon:stdio -m size=1G -kernel hougeos2.elf -S -gdb tcp::$(GDB_PORT)

gdb: hougeos2.elf
	gdb-multiarch -n -x .gdbinit

clean:
	/bin/rm $(BOOT_OFILES) $(KERN_OFILES) hougeos2.img hougeos2.elf hougeos2.S kern/kern.lds > /dev/null 2> /dev/null || true
